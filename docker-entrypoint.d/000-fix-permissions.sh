#!/bin/sh -e

chown allianceauth:allianceauth "/var/www"
find "/var/www" \! -user "allianceauth" -exec chown allianceauth:allianceauth '{}' +
chown allianceauth:allianceauth "/allianceauth/myauth/log"
find "/allianceauth/myauth/log" \! -user "allianceauth" -exec chown allianceauth:allianceauth '{}' +
