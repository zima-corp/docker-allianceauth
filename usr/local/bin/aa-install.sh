#!/bin/sh -x

DJANGO_MANAGE="python manage.py"
STATIC_DATA="/var/www/myauth/static"

cd "/allianceauth/myauth" || exit 1

# common commands
$DJANGO_MANAGE makemigrations --no-input
$DJANGO_MANAGE migrate --no-input
if [ -d "$STATIC_DATA" ]; then
	rm -rf "$STATIC_DATA"
fi
$DJANGO_MANAGE collectstatic --no-input
# hot fix for django-jet-reboot
for f in /var/www/myauth/static/jet/css/themes/*/base.css; do
	echo '.changeform-tabs-item a:empty::after{content:"More"}' >>"$f"
done

# see https://django-eveuniverse.readthedocs.io/en/latest/operations.html
$DJANGO_MANAGE eveuniverse_load_data map --no-input
$DJANGO_MANAGE eveuniverse_load_data ships --no-input
$DJANGO_MANAGE eveuniverse_load_data structures --no-input
$DJANGO_MANAGE eveuniverse_load_data types --no-input

# see https://github.com/ppfeufer/aa-srp
$DJANGO_MANAGE aasrp_load_eve --no-input

# see https://aa-structures.readthedocs.io/en/latest/operations.html
$DJANGO_MANAGE structures_load_eve --no-input

# see https://gitlab.com/ErikKalkoken/aa-memberaudit
$DJANGO_MANAGE memberaudit_load_eve

# see https://gitlab.com/ErikKalkoken/aa-moonmining
$DJANGO_MANAGE moonmining_load_eve

# see https://github.com/ppfeufer/aa-sov-timer
$DJANGO_MANAGE sovtimer_load_initial_data

# see https://gitlab.com/ErikKalkoken/aa-structuretimers
$DJANGO_MANAGE structuretimers_load_eve

# see https://gitlab.com/tactical-supremacy/aa-market-manager
$DJANGO_MANAGE marketmanager_preload_common_eve_types

# see https://github.com/Solar-Helix-Independent-Transport/allianceauth-blacklist
$DJANGO_MANAGE blacklist_state

# see https://gitlab.com/colcrunch/fittings
$DJANGO_MANAGE fittings_preload_data

# see https://gitlab.com/ErikKalkoken/aa-package-monitor
$DJANGO_MANAGE package_monitor_refresh

# see https://github.com/ppfeufer/aa-intel-tool
$DJANGO_MANAGE aa_intel_tool_load_eve_types

# see https://gitlab.com/paulipa/allianceauth-buyback-program
$DJANGO_MANAGE buybackprogram_load_data
$DJANGO_MANAGE buybackprogram_load_prices

$DJANGO_MANAGE check
