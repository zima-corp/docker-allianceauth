#!/bin/sh -x

cd /allianceauth/myauth || exit 1

# See
# https://github.com/ppfeufer/aa-srp
# https://github.com/ppfeufer/aa-srp#step-4---preload-eve-universe-data
python manage.py aasrp_load_eve
