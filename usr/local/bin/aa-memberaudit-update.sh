#!/bin/sh -x

cd /allianceauth/myauth || exit 1

# See
# https://aa-memberaudit.readthedocs.io/en/latest/operations.html#installation
# https://aa-memberaudit.readthedocs.io/en/latest/operations.html#management-commands
python manage.py memberaudit_update_characters
