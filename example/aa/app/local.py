# Every setting in base.py can be overloaded by redefining it here.
from .base import *

# These are required for Django to function properly. Don't touch.
ROOT_URLCONF = 'myauth.urls'
WSGI_APPLICATION = 'myauth.wsgi.application'
SECRET_KEY = 'django-insecure-onvdg4-fa==*oh7bdkh0b9z@7cirxr-a6mmqv3^g2tiya(^q$e'

# This is where css/images will be placed for your webserver to read
STATIC_ROOT = "/var/www/myauth/static/"

# Change this to change the name of the auth site displayed
# in page titles and the site header.
SITE_NAME = 'Zima Corp'

# This is your websites URL, set it accordingly
# Make sure this URL is WITHOUT a trailing slash
SITE_URL = "http://zimacorp.localdomain"

# Django security
CSRF_TRUSTED_ORIGINS = [SITE_URL]

# Change this to enable/disable debug mode, which displays
# useful error messages but can leak sensitive data.
DEBUG = True

# Add any additional apps to this list.
INSTALLED_APPS += [
  'allianceauth.eveonline.autogroups',
  'allianceauth.corputils',
  'allianceauth.permissions_tool',
  'afat',
  'fittings',
  'eveuniverse',
  'memberaudit',
  'memberaudit_securegroups',
  'aasrp'
]

# To change the logging level for extensions, uncomment the following line.
# LOGGING['handlers']['extension_file']['level'] = 'DEBUG'


# Enter credentials to use MySQL/MariaDB. Comment out to use sqlite3
DATABASES['default'] = {
    'ENGINE': 'django.db.backends.mysql',
    'NAME': 'allianceauth',
    'USER': 'allianceauth',
    'PASSWORD': 'Slx1xFXceGTDzavS',
    'HOST': 'allianceauth_mariadb',
    'PORT': '3306',
    'OPTIONS': {'charset': 'utf8mb4'},
}

# Register an application at https://developers.eveonline.com for Authentication
# & API Access and fill out these settings. Be sure to set the callback URL
# to https://example.com/sso/callback substituting your domain for example.com in
# CCP's developer portal
# Logging in to auth requires the publicData scope (can be overridden through the
# LOGIN_TOKEN_SCOPES setting). Other apps may require more (see their docs).
ESI_SSO_CLIENT_ID = '8d69b68b4d5f4994ade3f589de3c3a40'
ESI_SSO_CLIENT_SECRET = 'hmnKRczQEjuRXUpe4AFw1D5Y0RjnTYW3DmrEB5P8'
ESI_SSO_CALLBACK_URL = f"{SITE_URL}/sso/callback"
ESI_USER_CONTACT_EMAIL = ''    # A server maintainer that CCP can contact in case of issues.

# By default, emails are validated before new users can log in.
# It's recommended to use a free service like SparkPost or Elastic Email to send email.
# https://www.sparkpost.com/docs/integrations/django/
# https://elasticemail.com/resources/settings/smtp-api/
# Set the default from email to something like 'noreply@example.com'
# Email validation can be turned off by uncommenting the line below. This can break some services.
REGISTRATION_VERIFY_EMAIL = False
EMAIL_HOST = 'allianceauth_opensmtpd'
EMAIL_PORT = 25
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False
DEFAULT_FROM_EMAIL = 'noreply@zimacorp.localdomain'

# Cache compression can help on bigger auths where ram starts to become an issue.
# Uncomment the following 3 lines to enable.

#CACHES["default"]["OPTIONS"] = {
#    "COMPRESSOR": "django_redis.compressors.lzma.LzmaCompressor",
#}

#######################################
# Add any custom settings below here. #
#######################################
BROKER_URL = 'redis://127.0.0.1:6379/0'

# see https://allianceauth.readthedocs.io/en/latest/features/core/groups.html#settings
GROUPMANAGEMENT_REQUESTS_NOTIFICATION = True
# see https://allianceauth.readthedocs.io/en/latest/features/core/analytics.html#how-to-opt-out
ANALYTICS_DISABLED = True

# Alliance Auth AFAT - Another Fleet Activity Tracker
# see https://github.com/ppfeufer/allianceauth-afat
CELERYBEAT_SCHEDULE["afat_update_esi_fatlinks"] = {
    "task": "afat.tasks.update_esi_fatlinks",
    "schedule": crontab(minute="*"),
}
CELERYBEAT_SCHEDULE["afat_logrotate"] = {
    "task": "afat.tasks.logrotate",
    "schedule": crontab(minute="0", hour="1"),
}

# Fittings Module
# https://gitlab.com/colcrunch/fittings
CELERYBEAT_SCHEDULE['fittings_update_types'] = {
    'task': 'fittings.tasks.verify_server_version_and_update_types',
    'schedule': crontab(minute=0, hour='12'),
}

# Member Audit
# https://aa-memberaudit.readthedocs.io/en/latest/operations.html#installation
MEMBERAUDIT_APP_NAME='Проверка персонажей'
CELERYBEAT_SCHEDULE['memberaudit_run_regular_updates'] = {
    'task': 'memberaudit.tasks.run_regular_updates',
    'schedule': crontab(minute=0, hour='*/1'),
}
