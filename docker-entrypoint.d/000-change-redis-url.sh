#!/bin/sh -e

AA_REDIS_BASE_URL=${AA_REDIS_BASE_URL:-"redis://redis:6379"}

find -iname "base.py" -exec \
	sed -i -E \
	-e "s#redis://(localhost|127.0.0.1):6379/0#$AA_REDIS_BASE_URL/0#g" \
	-e "s#redis://(localhost|127.0.0.1):6379/1#$AA_REDIS_BASE_URL/1#g" {} +
