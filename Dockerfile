FROM python:3.12-alpine3.21

ARG ALLIANCEAUTH_GID=10000
ARG ALLIANCEAUTH_GROUP=allianceauth
ARG ALLIANCEAUTH_INSTALL_DIR=/allianceauth
ARG ALLIANCEAUTH_UID=10000
ARG ALLIANCEAUTH_USER=allianceauth
ARG ALLIANCEAUTH_VERSION=4.6.4

#
# Prepare environment
#
RUN \
  set -eux; \
  mkdir -p ${ALLIANCEAUTH_INSTALL_DIR}; \
  addgroup -g ${ALLIANCEAUTH_GID} -S ${ALLIANCEAUTH_GROUP}; \
  adduser -u ${ALLIANCEAUTH_UID} -D -S -G ${ALLIANCEAUTH_GROUP} -h ${ALLIANCEAUTH_INSTALL_DIR} ${ALLIANCEAUTH_USER}; \
  apk upgrade --no-cache; \
  apk add --no-cache \
  curl \
  mariadb-connector-c \
  msmtp \
  procps \
  tini \
  ;

#
# Build and install allianceauth with modules
#
# List community modules: https://gitlab.com/allianceauth/community-creations
# Install modules:
#  + https://github.com/Solar-Helix-Independent-Transport/allianceauth-blacklist
#  + https://github.com/Solar-Helix-Independent-Transport/allianceauth-wiki-js
#  + https://github.com/ppfeufer/aa-sov-timer
#  + https://github.com/ppfeufer/aa-srp
#  + https://github.com/ppfeufer/allianceauth-afat
#  + https://github.com/pvyParts/allianceauth-mumble-tagger
#  + https://github.com/pvyParts/allianceauth-mumble-temp
#  + https://gitlab.com/ErikKalkoken/aa-freight
#  + https://gitlab.com/ErikKalkoken/aa-memberaudit
#  + https://gitlab.com/ErikKalkoken/aa-moonmining
#  + https://gitlab.com/ErikKalkoken/aa-structures
#  + https://gitlab.com/ErikKalkoken/aa-structuretimers
#  + https://gitlab.com/ErikKalkoken/django-eveuniverse
#  + https://gitlab.com/colcrunch/fittings
#  + https://gitlab.com/eclipse-expeditions/aa-memberaudit-securegroups
#  + https://gitlab.com/paulipa/allianceauth-opcalendar
#  + https://gitlab.com/tactical-supremacy/aa-market-manager
# Install themes:
#  + https://gitlab.com/zima-corp/aa-theme-zima
RUN \
  set -eux; \
  apk add --no-cache --virtual .build-deps \
  bzip2-dev \
  cargo \
  gcc \
  git \
  libffi-dev \
  mariadb-connector-c-dev \
  openssl-dev \
  pkgconf \
  rust \
  unzip \
  ; \
  python --version; \
  pip --version; \
  pip install --no-cache-dir --disable-pip-version-check \
  aa-freight \
  aa-market-manager \
  aa-memberaudit \
  aa-memberaudit-securegroups \
  aa-moonmining \
  aa-opcalendar \
  aa-skip-email \
  aa-sov-timer \
  aa-srp \
  aa-structures \
  aa-structuretimers \
  aa-theme-zima \
  allianceauth==${ALLIANCEAUTH_VERSION} \
  allianceauth-afat \
  allianceauth-blacklist \
  allianceauth-mumble-tagger \
  allianceauth-mumbletemps \
  allianceauth-wiki-js \
  django-eveuniverse \
  fittings \
  flower \
  gunicorn \
  supervisor \
  wheel \
  ; \
  rm -rf /root/.cache; \
  rm -rf /root/.cargo; \
  apk del --no-network --purge .build-deps;

#
# Prepare allianceauth and configure supervisor
#
RUN \
  set -eux; \
  cd ${ALLIANCEAUTH_INSTALL_DIR}; \
  allianceauth start myauth; \
  chown -R ${ALLIANCEAUTH_USER}:${ALLIANCEAUTH_GROUP} ${ALLIANCEAUTH_INSTALL_DIR}; \
  supervisorConf="${ALLIANCEAUTH_INSTALL_DIR}/myauth/supervisor.conf"; \
  sed -i -E \
  -e '/directory\s*=/d' \
  -e '/user\s*=/d' \
  -e 's|(command\s*=\s*/.+/gunicorn\s.*)|\1 --bind 0.0.0.0:8000|g' \
  -e 's|(command\s*=\s*/.+/celery\s+-A\s+myauth\s+worker)\s+|\1 -l info --max-memory-per-child 524288 |g' \
  -e 's|(--concurrency)=5|\1=10|g' \
  -e 's|=256MB|=512MB|g' \
  ${supervisorConf}; \
  echo >> ${supervisorConf}; \
  echo "[include]" >> ${supervisorConf}; \
  echo "files=/usr/local/etc/supervisor/conf.d/*.conf" >> ${supervisorConf}; \
  mkdir -p /var/www/myauth/static; \
  chown -R ${ALLIANCEAUTH_USER}:${ALLIANCEAUTH_USER} /var/www/myauth/static;

COPY . /

WORKDIR ${ALLIANCEAUTH_INSTALL_DIR}/myauth

VOLUME /var/www
VOLUME ${ALLIANCEAUTH_INSTALL_DIR}/myauth/log

EXPOSE 8000
EXPOSE 5555

ENTRYPOINT ["tini", "--", "/docker-entrypoint.sh"]

CMD []
