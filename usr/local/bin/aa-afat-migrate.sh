#!/bin/sh -x

cd /allianceauth/myauth || exit 1

# See
# https://github.com/ppfeufer/allianceauth-afat
# https://github.com/ppfeufer/allianceauth-afat#data-migration
python manage.py afat_import_from_allianceauth_fat
python manage.py afat_import_from_imicusfat
