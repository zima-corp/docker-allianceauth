#!/bin/sh -x

cd /allianceauth/myauth || exit 1

# See
# https://gitlab.com/ErikKalkoken/aa-structuretimers
# https://gitlab.com/ErikKalkoken/aa-structuretimers#step-6-migrate-existing-timers
python manage.py structuretimers_migrate_timers
