#!/bin/sh -x

# see https://gitlab.com/ErikKalkoken/aa-moonmining#step-6-load-prices-from-esi
celery -A myauth call moonmining.tasks.run_calculated_properties_update
