#!/bin/sh -x

cd /allianceauth/myauth || exit 1

# See
# https://github.com/ppfeufer/aa-srp
# https://github.com/ppfeufer/aa-srp#step-5---set-up-permissions
python manage.py aasrp_migrate_srp_data
