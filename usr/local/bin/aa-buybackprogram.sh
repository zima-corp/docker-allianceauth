#!/bin/sh -x

cd /allianceauth/myauth || exit 1

# See
# https://gitlab.com/paulipa/allianceauth-buyback-program
python manage.py buybackprogram_load_data
python manage.py buybackprogram_load_prices
